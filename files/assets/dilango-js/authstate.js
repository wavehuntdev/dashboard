$( document ).ready(function() 
{

  firebase.auth().onAuthStateChanged(function(admin) 
  {
    if (admin) {
                  var displayName = admin.displayName;
                  var email = admin.email;
                  var emailVerified = admin.emailVerified;
                  var photoURL = admin.photoURL;
                  var isAnonymous = admin.isAnonymous;
                  var uid = admin.uid;
                  var providerData = admin.providerData;
                  var phoneNumber = admin.phoneNumber;
       
                  
                  db.collection("admin").doc(admin.uid).get().then(function (doc) {
                    var admindoc = doc.data()

                    if((admindoc.activeStatus == true) && (admindoc.adminState == true) && (admin.email == admindoc.email))
                    {
                      sessionStorage.setItem("dilango_admin", admin.email)
                      console.log('Authorized Admin')
                       $('#profile_name').text(admin.displayName)
                    }
                    else
                    {
                        console.log("Not Admin Account or Account Not Active ")
                        sessionStorage.setItem("dilango_admin", "Unavailable")

                        if (firebase.auth().currentUser) 
                        {
                        firebase.auth().signOut(); 
                        }
                        
                    }
                    

                  }).catch(function () {
                    console.log("id wrong")
                    sessionStorage.setItem("dilango_admin", "Unavailable")
                  })
                  

                } 
                else 
                {
                  console.log("Signed out")
                  sessionStorage.setItem("dilango_admin", "Unavailable")
                  
                }
  });

});
  
$("#btn_logout").click(function()
        {
        
        if (firebase.auth().currentUser) 
        {
        firebase.auth().signOut(); 
       
        $(window).attr('location','auth-normal-sign-in.html')   
        }


});





