$( document ).ready(function() 
{

var today = moment().format('YYYY-MM-DD');
$('#date_control').val(today);
$('#date_control_end').val(today);


db.collection("Dashboard").doc('count').get().then(function (docs) {

    var dashboard = docs.data()
    console.log(dashboard.count_vehicles_online)

    $("#count_bike").text(dashboard.count_bike)
    $("#count_tuk").text(dashboard.count_tuk)
    $("#count_nano").text(dashboard.count_nano)
    $("#count_sedan").text(dashboard.count_sedan)
    $("#count_mini").text(dashboard.count_mini)
    $("#count_minivan").text(dashboard.count_minivan)
    $("#count_van").text(dashboard.count_van)


    $("#count_bike_online").text(dashboard.count_bike_online)
    $("#count_nano_online").text(dashboard.count_nano_online)
    $("#count_mini_online").text(dashboard.count_mini_online)
    $("#count_sedan_online").text(dashboard.count_sedan_online)
    $("#count_van_online").text(dashboard.count_van_online)
    $("#count_minivan_online").text(dashboard.count_minivan_online)
    $("#count_tuk_online").text(dashboard.count_tuk_online)
    
    $('#count_total_drivers').text(dashboard.count_vehicles)
    $('#count_online_drivers').text(dashboard.count_vehicles_online)

    
    $('#count_total_alerts').text(dashboard.count_sos_alert)

    $('#count_total_rides').text(dashboard.count_rides)
    $('#count_total_road_hires').text(dashboard.count_roadhires)
    $('#count_total_customer_hires').text(dashboard.count_customerhires)
    $('#count_rides_completed').text(dashboard.count_completed)
    $('#count_rides_cancelled').text(dashboard.count_cancelled)
    $('#count_rides_pending').text(dashboard.count_pending)
    $('#count_rides_completed_customer').text(dashboard.count_customerhires_completed)
    $('#count_rides_completed_road').text(dashboard.count_customerhires_cancelled)

    $('#count_ontrip_drivers').text(dashboard.count_ontrip)

   
    $('#total_billable_amount').text(numeral(dashboard.count_total_billable_amount).format('0.0a'))                          
    $('#total_paid_amount').text(numeral(dashboard.count_total_paid_amount).format('0.0a'))

});



});
