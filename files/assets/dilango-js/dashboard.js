$( document ).ready(function() 
{

var today = moment().format('YYYY-MM-DD');
$('#date_control').val(today);
$('#date_control_end').val(today);



db.collection("vehicle_prices").get().then(function(querySnapshot) {
   
    querySnapshot.forEach(function(doc) {
        var data = doc.data()
       
        switch(data.vehicle_name){
            case "Bike":
                    $("#count_bike").text(data.stat)
                break
            case "Tuk":
                    $("#count_tuk").text(data.stat)
                   
                break
            case "Nano":
                    $("#count_nano").text(data.stat)
                break
            case "Sedan":
                    $("#count_sedan").text(data.stat)
                break
            case "Mini Car":
                    $("#count_mini").text(data.stat)
                break
            case "Mini Van":
                    $("#count_minivan").text(data.stat)
                break
            case "Van":
                    $("#count_van").text(data.stat)
                break
            case "Mini":
                    $("#count_bike").text(data.stat)
                break
        }
    })  
});

//dashboard-count//


//db.collection("vehicles").where("vehicle_type", "==", "Bike").get().then(function(doc) {  
  //  $("#count_bike").text(doc.size)     
//});
//db.collection("vehicles").where("vehicle_type", "==", "Nano").onSnapshot(function(querySnapshot) {  
  //  $("#count_nano").text(querySnapshot.size)     
//});
//db.collection("vehicles").where("vehicle_type", "==", "Mini Car").get().then(function(doc) {  
  //  $("#count_mini").text(doc.size)     
//});
//db.collection("vehicles").where("vehicle_type", "==", "Sedan").get().then(function(doc) {  
  //  $("#count_sedan").text(doc.size)  
//});
//db.collection("vehicles").where("vehicle_type", "==", "Van").get().then(function(doc) {  
  //  $("#count_van").text(doc.size)     
//});
//db.collection("vehicles").where("vehicle_type", "==", "Mini Van").onSnapshot(function(querySnapshot) {  
  //  $("#count_minivan").text(querySnapshot.size)     
//});
//db.collection("vehicles").where("vehicle_type", "==", "Tuk").get().then(function(doc) {  
  //  $("#count_tuk").text(doc.size)     
//});
//db.collection("vehicles").where("vehicle_type", "==", "Luxury Car").get().then(function(querySnapshot) {  
  //  $("#count_luxury").text(querySnapshot.size)     
//});

 db.collection("drivers").where("current_vehicletype", "==", "Bike").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_bike_online").text(querySnapshot.size)
 });

db.collection("drivers").where("current_vehicletype", "==", "Nano").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_nano_online").text(querySnapshot.size)
 });
db.collection("drivers").where("current_vehicletype", "==", "Mini Car").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_mini_online").text(querySnapshot.size)
 });

db.collection("drivers").where("current_vehicletype", "==", "Sedan").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_sedan_online").text(querySnapshot.size)
});

db.collection("drivers").where("current_vehicletype", "==", "Van").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_van_online").text(querySnapshot.size)
});

db.collection("drivers").where("current_vehicletype", "==", "Mini Van").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_minivan_online").text(querySnapshot.size)
});

db.collection("drivers").where("current_vehicletype", "==", "Tuk").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_tuk_online").text(querySnapshot.size)
});

db.collection("drivers").where("current_vehicletype", "==", "Luxury Car").where("online", "==", true).onSnapshot(function(querySnapshot) {     
    $("#count_luxury_online").text(querySnapshot.size)
});



db.collection("alert_system").get().then(function(doc) { 
    $("#count_total_alerts").text(doc.size)     
});

db.collection("drivers").get().then(function(doc) { 
    $("#count_total_drivers").text(doc.size)     
});

db.collection("drivers").where("online", "==", true).onSnapshot(function(querySnapshot) { 
    $("#count_online_drivers").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});

db.collection("drivers").where("ontrip", "==", true).onSnapshot(function(querySnapshot) { 
    $("#count_ontrip_drivers").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});





function todayTotalHires()
{
   
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');

    window.todayTotalHires = db.collection("rides").onSnapshot(function(doc) { 
        
        var count = 0 
        doc.forEach(function (snap){
                
                //if(moment(snap.data().create_time).format('L')==moment(selectDate).format('L'))
                //if((selectDate <= snap.data().create_time && snap.data().create_time <= endDate) || moment(snap.data().create_time).format('L')==moment(selectDate).format('L'))
                //if(snap.data().create_time >= selectDate)
                //if((snap.data().create_time >= selectDate) && (snap.data().create_time <= endDate))
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate))   
                {
                    //console.log(moment(snap.data().create_time).format('L')+"  -  "+moment().format('L'))
                    //console.log(moment(selectDate).format('L'))
                    count ++
                }
         })
        
        $("#today_total_hires").text(count) 
     });
}
todayTotalHires()

db.collection("rides").get().then(function(querySnapshot) { 
    $("#count_total_rides").text(querySnapshot.size)     
});




db.collection("rides").where("status", "==", "COMPLETED").onSnapshot(function(querySnapshot) { 
    $("#count_rides_completed").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});


function todayTotalCompleted()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');

    window.todayTotalCompleted = db.collection("rides").where("status", "==", "COMPLETED").onSnapshot(function(doc) {     
        
        var count = 0 
        doc.forEach(function (snap){
                
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {
                    //console.log(snap.data().create_time)
                    count ++
                }
         })
    
         $("#today_total_completed").text(count) 
         
     });
}
todayTotalCompleted()


db.collection("rides").where("status", "==", "COMPLETED").onSnapshot(function(querySnapshot) { 
    $("#count_rides_completed").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});





function todayTotalPending()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');

    window.todayTotalPending = db.collection("rides").where("status", "==", "PENDING").onSnapshot(function(doc) {     
        
        var count = 0 
        doc.forEach(function (snap){
                
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {
                    //console.log(snap.data().create_time)
                    count ++
                }
         })
    
         $("#today_total_pending").text(count) 
         
     });
}
todayTotalPending()

db.collection("rides").where("status", "==", "PENDING").onSnapshot(function(querySnapshot) { 
    $("#count_rides_pending").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});






function todatCancelledHires()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');

    window.todatCancelledHires = db.collection("rides").where("status", "==", "CANCELED").onSnapshot(function(doc) {     

        var count = 0 
        doc.forEach(function (snap){
                
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {
                    //console.log(moment(snap.data().create_time).format('L')+"  -  "+moment().format('L'))
                    //console.log(snap.data().create_time)
                    count ++
                }
         })
         
         $("#today_total_cancelled").text(count) 
         
     });
}
todatCancelledHires()




db.collection("rides").where("status", "==", "CANCELED").onSnapshot(function(querySnapshot) { 
    $("#count_rides_cancelled").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});



function todayCompletedRoaadHires()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');
    window.todayCompletedRoaadHires = db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "ROADHIRE").onSnapshot(function(doc) {     

        
        var count = 0 
        doc.forEach(function (snap){
                
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {
                    //console.log(snap.data().create_time)
                    count ++
                }
         })
         
         $('#today_total_completed_road').text(count)
         
     });
}
todayCompletedRoaadHires()

db.collection("rides").where("type", "==", "ROADHIRE").where("status", "==", "COMPLETED").onSnapshot(function(querySnapshot) { 
    $("#count_total_road_hires").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});





function todayCompletedCustomerHires()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');
    window.todayCompletedCustomerHires = db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "CUSTOMERHIRE").onSnapshot(function(doc) {     

        var count = 0 
        doc.forEach(function (snap){     
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {
                    //console.log(snap.data().create_time)
                    count ++
                }
         })
        
         $("#today_total_completed_customer").text(count)
    
     });
}
todayCompletedCustomerHires()


db.collection("rides").where("type", "==", "CUSTOMERHIRE").where("status", "==", "COMPLETED").onSnapshot(function(querySnapshot) { 
    $("#count_total_app_hires").text(querySnapshot.size)    
    //console.log("online user"+querySnapshot.size) 
});




function todayEarnings()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');

    window.todayEarnings = db.collection("rides").where("status", "==", "COMPLETED").onSnapshot(function(doc) {     

        var total = 0
    
         doc.forEach(function (snap){
    
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {   
                    //console.log(snap.data().amount)
                    if(snap.data().amount > 0){
                        total += parseInt(snap.data().amount)
                    }
                }
         })
    
        var total = numeral(total).format('0.0a');
        $("#today_total_earnings").text(total) 
         
     });
}
todayEarnings()



db.collection("rides").where("status", "==", "COMPLETED").onSnapshot(function(doc) {
        
   var total = 0
    doc.forEach(function (snap){

        if(snap.data().amount > 0){
            total += parseInt(snap.data().amount)
        }
    })
    var total = numeral(total).format('0.0a');
    $("#total_earnings").text(total) 
    
});


function todayEarningsRoad()
{
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');

    window.todayEarningsRoad = db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "ROADHIRE").onSnapshot(function(doc) {     

        var total = 0
    
         doc.forEach(function (snap){
    
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {   
                    //console.log(snap.data().amount)
                    if(snap.data().amount > 0){
                        total += parseInt(snap.data().amount)
                    }
                }
         })
    
    
        var total = numeral(total).format('0.0a');
        $('#today_total_earnings_road').text(total)
         
     });
}
todayEarningsRoad()


function today_total_earnings_customer()
{
    
    var selectDate = $('#date_control').val()
    var endDate = $('#date_control_end').val()
    var new_enddate = moment(endDate).add(1, 'days');
    
    window.today_total_earnings_customer = db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "CUSTOMERHIRE").onSnapshot(function(doc) {     

        var total = 0
    
         doc.forEach(function (snap){
    
                if(moment(snap.data().create_time).isBetween(selectDate, new_enddate)) 
                {   
                    //console.log(snap.data().amount)
                    if(snap.data().amount > 0){
                        total += parseInt(snap.data().amount)
                    }
                }
         })
    
    
        var total = numeral(total).format('0.0a');
        $('#today_total_earnings_customer').text(total)
         
     });
}
today_total_earnings_customer()
 



function snapshot_func()
{
    window.todayTotalHires()
    window.todayTotalCompleted()
    window.todatCancelledHires()
    window.todayTotalPending()
    window.todayCompletedRoaadHires()
    window.todayCompletedCustomerHires()
    window.todayEarnings()
    window.todayEarningsRoad()
    window.today_total_earnings_customer()

    $("#today_total_hires").html('<div class="preloader3 loader-block" style="height:6px;margin-top:30px;;"><div class="circ1 loader-info"></div><div class="circ2 loader-info"></div><div class="circ3 loader-info"></div><div class="circ4 loader-info"></div></div>') 
    $("#today_total_completed").html('<div class="preloader3 loader-block" style="height:6px;margin-top:30px;;"><div class="circ1 loader-info"></div><div class="circ2 loader-info"></div><div class="circ3 loader-info"></div><div class="circ4 loader-info"></div></div>') 
    $("#today_total_cancelled").html('<div class="preloader3 loader-block" style="height:6px;margin-top:30px;;"><div class="circ1 loader-info"></div><div class="circ2 loader-info"></div><div class="circ3 loader-info"></div><div class="circ4 loader-info"></div></div>') 
    $("#today_total_pending").html('<div class="preloader3 loader-block" style="height:6px;margin-top:30px;;"><div class="circ1 loader-info"></div><div class="circ2 loader-info"></div><div class="circ3 loader-info"></div><div class="circ4 loader-info"></div></div>') 
    $("#today_total_completed_road").html('<div class="preloader3 loader-block" style="height:6px;margin-top:30px;;"><div class="circ1 loader-info"></div><div class="circ2 loader-info"></div><div class="circ3 loader-info"></div><div class="circ4 loader-info"></div></div>') 
    $("#today_total_completed_customer").html('<div class="preloader3 loader-block" style="height:6px;margin-top:30px;;"><div class="circ1 loader-info"></div><div class="circ2 loader-info"></div><div class="circ3 loader-info"></div><div class="circ4 loader-info"></div></div>') 
    $('#today_total_earnings').html('...')
    $('#today_total_earnings_road').html('...')
    $('#today_total_earnings_customer').html('...')
    todayTotalHires()
    todayTotalCompleted()
    todatCancelledHires()
    todayTotalPending()
    todayCompletedRoaadHires()
    todayCompletedCustomerHires()
    todayEarnings()
    todayEarningsRoad()
    today_total_earnings_customer()
}

function getMonthDate()
{
    var datePicker = new Date($('#date_control_month').val());
        var year = datePicker.getFullYear()
        var month = datePicker.getMonth() + 1

        var startDate = moment([year, month - 1, 01]).format("YYYY-MM-DD")
        var daysInMonth = moment(startDate).daysInMonth()
        var endDate = moment(startDate).add(daysInMonth - 1, 'days').format("YYYY-MM-DD");

        $('#date_control').val(startDate);
        $('#date_control_end').val(endDate);

        snapshot_func()

        console.log(startDate)
        console.log(endDate)
}

 $("#date_control, #date_control_end").change(function(){
    snapshot_func()
});


$("#date_control_month").change(function(){
    getMonthDate()      
});
$("#date_control_month").click(function(){
    getMonthDate()      
});

db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "ROADHIRE").onSnapshot(function(doc) {
        
    var total = 0
     doc.forEach(function (snap){
 
         if(snap.data().amount > 0){
             total += parseInt(snap.data().amount)
         }
     })
     var total = numeral(total).format('0.0a');
     $("#total_earnings_roadhires").text(total) 
     
 });






 db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "ROADHIRE").where("payment_mode", "==", "CASH").onSnapshot(function(doc) {
        
    var total = 0
     doc.forEach(function (snap){
 
         if(snap.data().amount > 0){
             total += parseInt(snap.data().amount)
         }
     })
     var total = numeral(total).format('0.0a');
     $("#total_earnings_cash").text(total) 
     
 });


 db.collection("rides").where("status", "==", "COMPLETED").where("type", "==", "ROADHIRE").where("payment_mode", "==", "CARD").onSnapshot(function(doc) {     
    var total = 0
     doc.forEach(function (snap){
 
         if(snap.data().amount > 0){
             total += parseInt(snap.data().amount)
         }
     })
     var total = numeral(total).format('0.0a');
     $("#total_earnings_card").text(total) 
     
 });
 

 



});
