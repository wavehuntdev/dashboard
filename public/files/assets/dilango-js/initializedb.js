var config = {
    apiKey: "AIzaSyBRBuy11rqclv_Iv9w48XX3Qmet0mPurgY",
    authDomain: "dilangotaxi-77df1.firebaseapp.com",
    databaseURL: "https://dilangotaxi-77df1.firebaseio.com",
    projectId: "dilangotaxi-77df1",
    storageBucket: "dilangotaxi-77df1.appspot.com",
    messagingSenderId: "490369108354",
    timestampsInSnapshots: true
};



firebase.initializeApp(config);
var db = firebase.firestore();
// Disable deprecated features
db.settings({
    timestampsInSnapshots: true
});

firebase.firestore().enablePersistence()
    .catch(function (err) {
        if (err.code == 'failed-precondition') {
            // Multiple tabs open, persistence can only be enabled
            // in one tab at a a time.
            // ...
        } else if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
            // ...
        }
    });