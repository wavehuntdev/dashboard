

function clearalerts() {
       
  $('#alert_password_not_match').hide()
  $('#alert_weak_password').hide()
  $('#alert_email_already_in_use').hide()
  $('#alert_invalid_email').hide()
  $('#alert_no_account').hide()
  $('#alert_wrong_password').hide()
  $('#alert_no_account').hide()

}
$("#quickstart-sign-up").click(function(){   clearalerts()

        console.log("clicked")
      
        username = $("#username").val();
        email = $("#email").val();
        password = $("#password").val();
        confirmpassword = $("#confirmpassword").val();
        mobile = $("#mobile").val();

        if(username.length < 4)
        {
          $('#username').focus()
          return
        }
        if(mobile.length < 10 || mobile.length > 10 )
        {
          $('#mobile').focus()
          return
        }

        if(email.length < 4)
        {
          $('#email').focus()
          return
        }

        if(password.length < 4)
        {
          $('#password').focus()
          return
        }

        if(confirmpassword.length < 4)
        {
          $('#confirmpassword').focus()
          return
        }
        
        if(confirmpassword != password)
        {
          $('#alert_password_not_match').show()

          return
        }

    
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function() {
            
            $('#alert_account_success').show()

            firebase.auth().currentUser.sendEmailVerification().then(function() 
            {
              console.log('Email Verification Sent!');
              $('#alert_verify_email').show()
            });


            db.collection("admin").doc(firebase.auth().getUid()).set({
            displayName: username,
            email: email,
            password: password,
            phoneNumber: mobile
          })
            .then(function(admin) {
            var admin = firebase.auth().currentUser;
            //logUser(user); // Optional
    
            admin.updateProfile({
              displayName: username,
              phoneNumber: mobile
              
            }).then(function() {
               console.log("Update successful")
              //app.router.navigate('/login/');
            }).catch(function(error) {
              // An error happened.
            });
    
            if (admin != null) {
              admin.providerData.forEach(function (profile) {
                console.log("Sign-in provider: " + profile.providerId);
                console.log("  Provider-specific UID: " + profile.uid);
                console.log("  Name: " + profile.displayName);
                console.log("  Email: " + profile.email);
                console.log("  Photo URL: " + profile.photoURL);
                console.log("  Photo URL: " + profile.phoneNumber);

                if (firebase.auth().currentUser) 
                {
                firebase.auth().signOut(); 
                }

              });
            }
            //
          }, function(error) {
            
            var errorCode = error.code;
            var errorMessage = error.message;
            
            // ...
          });
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode)
              if (errorCode == 'auth/weak-password')              {  $('#alert_weak_password').show()
              } 
              else if (errorCode == 'auth/email-already-in-use')  {  $('#alert_email_already_in_use').show()
              }
              else if (errorCode == 'auth/invalid-email')         {  $('#alert_invalid_email').show()
              } 
              else                                                {  Console.log(errorMessage);  console.log(currentUser);
              }
            
        });
})



$("#btn_login").click(function (){   clearalerts()

  console.log("Login Clicked")

  email = $("#txt_email").val()
  password = $("#txt_password").val()

  firebase.auth().signInWithEmailAndPassword(email, password).then(function(firebaseUser) {
    // Success 
    var admin = firebase.auth().currentUser;

    //app.router.navigate('/category/');

  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code
    var errorMessage = error.message
    switch(errorCode){
      case "auth/invalid-email":
          $('#alert_invalid_email').show()
        break;
      case "auth/wrong-password":
          $('#alert_wrong_password').show()
        break;
      case "auth/user-not-found":
          $('#alert_no_account').show()
        //$("#login_error").html("<span style='color:blue'>There is no such account <a href='/register/'>Click here</a> to register</span>")
        break;
    }
    
    // ...
  });

})

$( document ).ready(function() 
{

  firebase.auth().onAuthStateChanged(function(admin) 
  {
    if (admin) {
                  var displayName = admin.displayName;
                  var email = admin.email;
                  var emailVerified = admin.emailVerified;
                  var photoURL = admin.photoURL;
                  var isAnonymous = admin.isAnonymous;
                  var uid = admin.uid;
                  var providerData = admin.providerData;
                  var phoneNumber = admin.phoneNumber;
                  sessionStorage.setItem("dilango_admin", admin.email)

                  console.log(displayName)
                  console.log(email)
                  console.log(emailVerified)
                  console.log(photoURL)
                  console.log(isAnonymous)
                  console.log(uid)
                  console.log(providerData)
                  console.log(phoneNumber)
                  pagedirect()
                  

                } 
                else 
                {
                  console.log("Signed out")
                  sessionStorage.setItem("dilango_admin", "Unavailable")
                }
  });

});

$("#quickstart-sign-out").click(function()
{
  if (firebase.auth().currentUser) 
  {
   firebase.auth().signOut(); 
  }

});


function pagedirect()
{ console.log(sessionStorage.getItem("dilango_admin"))

  if(sessionStorage.getItem("dilango_admin")=="Unavailable")
  {
    
  }
  else
  {
    $(window).attr('location','index.html')  
  }

}

$( document ).ready(function() 
{
  pagedirect()
});




